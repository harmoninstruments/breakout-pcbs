EESchema Schematic File Version 4
LIBS:USB-C_breakout-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "USB C Breakout"
Date "2019-02-01"
Rev "1"
Comp "Harmon Instruments, LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L connector:8RYXT J1
U 1 1 5C548E84
P 2650 2750
F 0 "J1" H 2250 2900 50  0000 L CNN
F 1 "8RYXT" H 2250 2800 50  0000 L CNN
F 2 "Connector_USB:USB_C_Receptacle_JAE_DX07S024WJ3R400" H 2250 2800 50  0001 L CNN
F 3 "" V 2500 2750 50  0001 C CNN
	1    2650 2750
	1    0    0    -1  
$EndComp
$Comp
L connector:coax_compression J2
U 1 1 5C549D12
P 3950 3200
F 0 "J2" H 3950 3350 50  0000 C CNN
F 1 "coax_compression" H 3950 3350 50  0001 C CNN
F 2 "coax_compression" H 3950 3350 50  0001 C CNN
F 3 "" H 3950 3200 50  0001 C CNN
	1    3950 3200
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0101
U 1 1 5C54AF7F
P 3950 3400
F 0 "#PWR0101" H 3950 3400 50  0001 C CNN
F 1 "ground" H 3950 3330 50  0001 C CNN
F 2 "" H 3950 3400 50  0001 C CNN
F 3 "" H 3950 3400 50  0000 C CNN
	1    3950 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 3550 3550 3550
Wire Wire Line
	3550 3550 3550 3200
Wire Wire Line
	3550 3200 3800 3200
$Comp
L connector:coax_compression J3
U 1 1 5C54B638
P 3950 3650
F 0 "J3" H 3950 3800 50  0000 C CNN
F 1 "coax_compression" H 3950 3800 50  0001 C CNN
F 2 "coax_compression" H 3950 3800 50  0001 C CNN
F 3 "" H 3950 3650 50  0001 C CNN
	1    3950 3650
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0102
U 1 1 5C54B642
P 3950 3850
F 0 "#PWR0102" H 3950 3850 50  0001 C CNN
F 1 "ground" H 3950 3780 50  0001 C CNN
F 2 "" H 3950 3850 50  0001 C CNN
F 3 "" H 3950 3850 50  0000 C CNN
	1    3950 3850
	1    0    0    -1  
$EndComp
$Comp
L connector:coax_compression J4
U 1 1 5C54C018
P 3950 4150
F 0 "J4" H 3950 4300 50  0000 C CNN
F 1 "coax_compression" H 3950 4300 50  0001 C CNN
F 2 "coax_compression" H 3950 4300 50  0001 C CNN
F 3 "" H 3950 4150 50  0001 C CNN
	1    3950 4150
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0103
U 1 1 5C54C022
P 3950 4350
F 0 "#PWR0103" H 3950 4350 50  0001 C CNN
F 1 "ground" H 3950 4280 50  0001 C CNN
F 2 "" H 3950 4350 50  0001 C CNN
F 3 "" H 3950 4350 50  0000 C CNN
	1    3950 4350
	1    0    0    -1  
$EndComp
$Comp
L connector:coax_compression J5
U 1 1 5C54C02C
P 3950 4600
F 0 "J5" H 3950 4750 50  0000 C CNN
F 1 "coax_compression" H 3950 4750 50  0001 C CNN
F 2 "coax_compression" H 3950 4750 50  0001 C CNN
F 3 "" H 3950 4600 50  0001 C CNN
	1    3950 4600
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0104
U 1 1 5C54C036
P 3950 4800
F 0 "#PWR0104" H 3950 4800 50  0001 C CNN
F 1 "ground" H 3950 4730 50  0001 C CNN
F 2 "" H 3950 4800 50  0001 C CNN
F 3 "" H 3950 4800 50  0000 C CNN
	1    3950 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 4150 3750 4150
Wire Wire Line
	3750 4150 3750 3750
Wire Wire Line
	3750 3750 2800 3750
Wire Wire Line
	3800 4600 3650 4600
Wire Wire Line
	3650 4600 3650 3850
Wire Wire Line
	3650 3850 2800 3850
$Comp
L connector:coax_compression J6
U 1 1 5C54DDF3
P 3950 5050
F 0 "J6" H 3950 5200 50  0000 C CNN
F 1 "coax_compression" H 3950 5200 50  0001 C CNN
F 2 "coax_compression" H 3950 5200 50  0001 C CNN
F 3 "" H 3950 5050 50  0001 C CNN
	1    3950 5050
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0105
U 1 1 5C54DDFD
P 3950 5250
F 0 "#PWR0105" H 3950 5250 50  0001 C CNN
F 1 "ground" H 3950 5180 50  0001 C CNN
F 2 "" H 3950 5250 50  0001 C CNN
F 3 "" H 3950 5250 50  0000 C CNN
	1    3950 5250
	1    0    0    -1  
$EndComp
$Comp
L connector:coax_compression J7
U 1 1 5C54DE07
P 3950 5500
F 0 "J7" H 3950 5650 50  0000 C CNN
F 1 "coax_compression" H 3950 5650 50  0001 C CNN
F 2 "coax_compression" H 3950 5650 50  0001 C CNN
F 3 "" H 3950 5500 50  0001 C CNN
	1    3950 5500
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0106
U 1 1 5C54DE11
P 3950 5700
F 0 "#PWR0106" H 3950 5700 50  0001 C CNN
F 1 "ground" H 3950 5630 50  0001 C CNN
F 2 "" H 3950 5700 50  0001 C CNN
F 3 "" H 3950 5700 50  0000 C CNN
	1    3950 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 5050 3550 5050
Wire Wire Line
	3550 5050 3550 3950
Wire Wire Line
	3550 3950 2800 3950
Wire Wire Line
	3800 5500 3450 5500
Wire Wire Line
	3450 5500 3450 4050
Wire Wire Line
	3450 4050 2800 4050
$Comp
L connector:coax_compression J8
U 1 1 5C550089
P 3950 5950
F 0 "J8" H 3950 6100 50  0000 C CNN
F 1 "coax_compression" H 3950 6100 50  0001 C CNN
F 2 "coax_compression" H 3950 6100 50  0001 C CNN
F 3 "" H 3950 5950 50  0001 C CNN
	1    3950 5950
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0107
U 1 1 5C550093
P 3950 6150
F 0 "#PWR0107" H 3950 6150 50  0001 C CNN
F 1 "ground" H 3950 6080 50  0001 C CNN
F 2 "" H 3950 6150 50  0001 C CNN
F 3 "" H 3950 6150 50  0000 C CNN
	1    3950 6150
	1    0    0    -1  
$EndComp
$Comp
L connector:coax_compression J9
U 1 1 5C55009D
P 3950 6400
F 0 "J9" H 3950 6550 50  0000 C CNN
F 1 "coax_compression" H 3950 6550 50  0001 C CNN
F 2 "coax_compression" H 3950 6550 50  0001 C CNN
F 3 "" H 3950 6400 50  0001 C CNN
	1    3950 6400
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0108
U 1 1 5C5500A7
P 3950 6600
F 0 "#PWR0108" H 3950 6600 50  0001 C CNN
F 1 "ground" H 3950 6530 50  0001 C CNN
F 2 "" H 3950 6600 50  0001 C CNN
F 3 "" H 3950 6600 50  0000 C CNN
	1    3950 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 5950 3350 5950
Wire Wire Line
	3350 5950 3350 4150
Wire Wire Line
	3350 4150 2800 4150
Wire Wire Line
	3800 6400 3250 6400
Wire Wire Line
	3250 6400 3250 4250
Wire Wire Line
	3250 4250 2800 4250
$Comp
L combined:ground #PWR0109
U 1 1 5C553096
P 2850 4400
F 0 "#PWR0109" H 2850 4400 50  0001 C CNN
F 1 "ground" H 2850 4330 50  0001 C CNN
F 2 "" H 2850 4400 50  0001 C CNN
F 3 "" H 2850 4400 50  0000 C CNN
	1    2850 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 4400 2850 4350
Wire Wire Line
	2850 4350 2800 4350
Wire Wire Line
	2800 2850 3150 2850
Text Label 3150 2850 2    50   ~ 0
VBUS
Text Label 2850 3550 0    50   ~ 0
TX1+
Text Label 2850 3650 0    50   ~ 0
TX1-
Text Label 2850 3750 0    50   ~ 0
TX2+
Text Label 2850 3850 0    50   ~ 0
TX2-
Text Label 2850 3950 0    50   ~ 0
RX1+
Text Label 2850 4050 0    50   ~ 0
RX1-
Text Label 2850 4150 0    50   ~ 0
RX2+
Text Label 2850 4250 0    50   ~ 0
RX2-
$Comp
L misc:testpoint_1mm TP4
U 1 1 5C55B7CA
P 3200 3250
F 0 "TP4" H 3350 3050 50  0000 L CNN
F 1 "testpoint_1mm" H 3400 3050 50  0001 L CNN
F 2 "kicad_pcb:TP_1mm" H 3400 3150 50  0001 L CNN
F 3 "" V 3450 3250 50  0001 C CNN
	1    3200 3250
	1    0    0    -1  
$EndComp
$Comp
L misc:testpoint_1mm TP3
U 1 1 5C55C1B5
P 3200 3150
F 0 "TP3" H 3350 2950 50  0000 L CNN
F 1 "testpoint_1mm" H 3400 2950 50  0001 L CNN
F 2 "kicad_pcb:TP_1mm" H 3400 3050 50  0001 L CNN
F 3 "" V 3450 3150 50  0001 C CNN
	1    3200 3150
	1    0    0    -1  
$EndComp
$Comp
L misc:testpoint_1mm TP2
U 1 1 5C55C409
P 3200 3050
F 0 "TP2" H 3350 2850 50  0000 L CNN
F 1 "testpoint_1mm" H 3400 2850 50  0001 L CNN
F 2 "kicad_pcb:TP_1mm" H 3400 2950 50  0001 L CNN
F 3 "" V 3450 3050 50  0001 C CNN
	1    3200 3050
	1    0    0    -1  
$EndComp
$Comp
L misc:testpoint_1mm TP1
U 1 1 5C55C413
P 3200 2950
F 0 "TP1" H 3350 2750 50  0000 L CNN
F 1 "testpoint_1mm" H 3400 2750 50  0001 L CNN
F 2 "kicad_pcb:TP_1mm" H 3400 2850 50  0001 L CNN
F 3 "" V 3450 2950 50  0001 C CNN
	1    3200 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3150 2800 3150
Wire Wire Line
	3100 3250 2800 3250
Wire Wire Line
	3100 3350 2800 3350
Wire Wire Line
	3100 3450 2800 3450
Text Label 2850 3150 0    50   ~ 0
CC1
Text Label 2850 3250 0    50   ~ 0
CC2
Text Label 2850 3350 0    50   ~ 0
SBU1
Text Label 2850 3450 0    50   ~ 0
SBU2
$Comp
L misc:testpoint_1mm TP5
U 1 1 5C561C62
P 3250 2650
F 0 "TP5" H 3400 2450 50  0000 L CNN
F 1 "testpoint_1mm" H 3450 2450 50  0001 L CNN
F 2 "kicad_pcb:TP_1mm" H 3450 2550 50  0001 L CNN
F 3 "" V 3500 2650 50  0001 C CNN
	1    3250 2650
	1    0    0    -1  
$EndComp
$Comp
L connector:coax_compression J11
U 1 1 5C562BB0
P 4750 3050
F 0 "J11" H 4750 3200 50  0000 C CNN
F 1 "coax_compression" H 4750 3200 50  0001 C CNN
F 2 "coax_compression" H 4750 3200 50  0001 C CNN
F 3 "" H 4750 3050 50  0001 C CNN
	1    4750 3050
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0110
U 1 1 5C562BBA
P 4750 3250
F 0 "#PWR0110" H 4750 3250 50  0001 C CNN
F 1 "ground" H 4750 3180 50  0001 C CNN
F 2 "" H 4750 3250 50  0001 C CNN
F 3 "" H 4750 3250 50  0000 C CNN
	1    4750 3250
	1    0    0    -1  
$EndComp
$Comp
L connector:coax_compression J10
U 1 1 5C563693
P 4450 3050
F 0 "J10" H 4450 3200 50  0000 C CNN
F 1 "coax_compression" H 4450 3200 50  0001 C CNN
F 2 "coax_compression" H 4450 3200 50  0001 C CNN
F 3 "" H 4450 3050 50  0001 C CNN
	1    4450 3050
	-1   0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0111
U 1 1 5C56369D
P 4450 3250
F 0 "#PWR0111" H 4450 3250 50  0001 C CNN
F 1 "ground" H 4450 3180 50  0001 C CNN
F 2 "" H 4450 3250 50  0001 C CNN
F 3 "" H 4450 3250 50  0000 C CNN
	1    4450 3250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2800 3650 3800 3650
$EndSCHEMATC
