EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "21 pin 0.3 mm pitch FPC breakout"
Date "2019-02-02"
Rev "1"
Comp "Harmon Instruments, LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L connector:coax_compression J2
U 1 1 5C549D12
P 3950 3250
F 0 "J2" H 3950 3400 50  0000 C CNN
F 1 "coax_compression" H 3950 3400 50  0001 C CNN
F 2 "coax_compression" H 3950 3400 50  0001 C CNN
F 3 "" H 3950 3250 50  0001 C CNN
	1    3950 3250
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0101
U 1 1 5C54AF7F
P 3950 3450
F 0 "#PWR0101" H 3950 3450 50  0001 C CNN
F 1 "ground" H 3950 3380 50  0001 C CNN
F 2 "" H 3950 3450 50  0001 C CNN
F 3 "" H 3950 3450 50  0000 C CNN
	1    3950 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 3600 3550 3600
Wire Wire Line
	3550 3250 3800 3250
$Comp
L connector:coax_compression J3
U 1 1 5C54B638
P 3950 3700
F 0 "J3" H 3950 3850 50  0000 C CNN
F 1 "coax_compression" H 3950 3850 50  0001 C CNN
F 2 "coax_compression" H 3950 3850 50  0001 C CNN
F 3 "" H 3950 3700 50  0001 C CNN
	1    3950 3700
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0102
U 1 1 5C54B642
P 3950 3900
F 0 "#PWR0102" H 3950 3900 50  0001 C CNN
F 1 "ground" H 3950 3830 50  0001 C CNN
F 2 "" H 3950 3900 50  0001 C CNN
F 3 "" H 3950 3900 50  0000 C CNN
	1    3950 3900
	1    0    0    -1  
$EndComp
$Comp
L connector:coax_compression J4
U 1 1 5C54C018
P 3950 4150
F 0 "J4" H 3950 4300 50  0000 C CNN
F 1 "coax_compression" H 3950 4300 50  0001 C CNN
F 2 "coax_compression" H 3950 4300 50  0001 C CNN
F 3 "" H 3950 4150 50  0001 C CNN
	1    3950 4150
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0103
U 1 1 5C54C022
P 3950 4350
F 0 "#PWR0103" H 3950 4350 50  0001 C CNN
F 1 "ground" H 3950 4280 50  0001 C CNN
F 2 "" H 3950 4350 50  0001 C CNN
F 3 "" H 3950 4350 50  0000 C CNN
	1    3950 4350
	1    0    0    -1  
$EndComp
$Comp
L connector:coax_compression J5
U 1 1 5C54C02C
P 3950 4600
F 0 "J5" H 3950 4750 50  0000 C CNN
F 1 "coax_compression" H 3950 4750 50  0001 C CNN
F 2 "coax_compression" H 3950 4750 50  0001 C CNN
F 3 "" H 3950 4600 50  0001 C CNN
	1    3950 4600
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0104
U 1 1 5C54C036
P 3950 4800
F 0 "#PWR0104" H 3950 4800 50  0001 C CNN
F 1 "ground" H 3950 4730 50  0001 C CNN
F 2 "" H 3950 4800 50  0001 C CNN
F 3 "" H 3950 4800 50  0000 C CNN
	1    3950 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 4150 3750 4150
Wire Wire Line
	3750 4150 3750 3800
Wire Wire Line
	3750 3800 2800 3800
Wire Wire Line
	3800 4600 3650 4600
Wire Wire Line
	3650 4600 3650 3900
Wire Wire Line
	3650 3900 2800 3900
$Comp
L connector:coax_compression J6
U 1 1 5C54DDF3
P 3950 5050
F 0 "J6" H 3950 5200 50  0000 C CNN
F 1 "coax_compression" H 3950 5200 50  0001 C CNN
F 2 "coax_compression" H 3950 5200 50  0001 C CNN
F 3 "" H 3950 5050 50  0001 C CNN
	1    3950 5050
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0105
U 1 1 5C54DDFD
P 3950 5250
F 0 "#PWR0105" H 3950 5250 50  0001 C CNN
F 1 "ground" H 3950 5180 50  0001 C CNN
F 2 "" H 3950 5250 50  0001 C CNN
F 3 "" H 3950 5250 50  0000 C CNN
	1    3950 5250
	1    0    0    -1  
$EndComp
$Comp
L connector:coax_compression J7
U 1 1 5C54DE07
P 3950 5500
F 0 "J7" H 3950 5650 50  0000 C CNN
F 1 "coax_compression" H 3950 5650 50  0001 C CNN
F 2 "coax_compression" H 3950 5650 50  0001 C CNN
F 3 "" H 3950 5500 50  0001 C CNN
	1    3950 5500
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0106
U 1 1 5C54DE11
P 3950 5700
F 0 "#PWR0106" H 3950 5700 50  0001 C CNN
F 1 "ground" H 3950 5630 50  0001 C CNN
F 2 "" H 3950 5700 50  0001 C CNN
F 3 "" H 3950 5700 50  0000 C CNN
	1    3950 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 5050 3550 5050
Wire Wire Line
	3550 5050 3550 4000
Wire Wire Line
	3550 4000 2800 4000
Wire Wire Line
	3800 5500 3450 5500
Wire Wire Line
	3450 5500 3450 4100
Wire Wire Line
	3450 4100 2800 4100
$Comp
L connector:coax_compression J8
U 1 1 5C550089
P 3950 5950
F 0 "J8" H 3950 6100 50  0000 C CNN
F 1 "coax_compression" H 3950 6100 50  0001 C CNN
F 2 "coax_compression" H 3950 6100 50  0001 C CNN
F 3 "" H 3950 5950 50  0001 C CNN
	1    3950 5950
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0107
U 1 1 5C550093
P 3950 6150
F 0 "#PWR0107" H 3950 6150 50  0001 C CNN
F 1 "ground" H 3950 6080 50  0001 C CNN
F 2 "" H 3950 6150 50  0001 C CNN
F 3 "" H 3950 6150 50  0000 C CNN
	1    3950 6150
	1    0    0    -1  
$EndComp
$Comp
L connector:coax_compression J9
U 1 1 5C55009D
P 3950 6400
F 0 "J9" H 3950 6550 50  0000 C CNN
F 1 "coax_compression" H 3950 6550 50  0001 C CNN
F 2 "coax_compression" H 3950 6550 50  0001 C CNN
F 3 "" H 3950 6400 50  0001 C CNN
	1    3950 6400
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR0108
U 1 1 5C5500A7
P 3950 6600
F 0 "#PWR0108" H 3950 6600 50  0001 C CNN
F 1 "ground" H 3950 6530 50  0001 C CNN
F 2 "" H 3950 6600 50  0001 C CNN
F 3 "" H 3950 6600 50  0000 C CNN
	1    3950 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 5950 3350 5950
Wire Wire Line
	3350 5950 3350 4200
Wire Wire Line
	3350 4200 2800 4200
Wire Wire Line
	3800 6400 3250 6400
Wire Wire Line
	3250 6400 3250 4300
Wire Wire Line
	3250 4300 2800 4300
$Comp
L combined:ground #PWR0109
U 1 1 5C553096
P 2050 4750
F 0 "#PWR0109" H 2050 4750 50  0001 C CNN
F 1 "ground" H 2050 4680 50  0001 C CNN
F 2 "" H 2050 4750 50  0001 C CNN
F 3 "" H 2050 4750 50  0000 C CNN
	1    2050 4750
	1    0    0    -1  
$EndComp
Text Label 2850 3600 0    50   ~ 0
D1+
Text Label 2850 3700 0    50   ~ 0
D1-
Text Label 2850 3800 0    50   ~ 0
D2+
Text Label 2850 3900 0    50   ~ 0
D2-
Text Label 2850 4000 0    50   ~ 0
D3+
Text Label 2850 4100 0    50   ~ 0
D3-
Text Label 2850 4200 0    50   ~ 0
D4+
Text Label 2850 4300 0    50   ~ 0
D4-
$Comp
L misc:testpoint_1mm TP1
U 1 1 5C55C413
P 3150 3050
F 0 "TP1" H 3300 2850 50  0000 L CNN
F 1 "testpoint_1mm" H 3350 2850 50  0001 L CNN
F 2 "kicad_pcb:TP_1mm" H 3350 2950 50  0001 L CNN
F 3 "" V 3400 3050 50  0001 C CNN
	1    3150 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3250 2900 3250
Text Label 2950 3250 0    50   ~ 0
P1
$Comp
L connector:KQWQM J1
U 1 1 5C55CED9
P 2350 3350
F 0 "J1" H 2500 3400 50  0000 C CNN
F 1 "KQWQM" H 2350 3400 50  0001 L CNN
F 2 "kicad_pcb:fpc_0.3_21_socket" H 2350 3400 50  0001 L CNN
F 3 "$HOME/parts/id/KQWQM/ENG_CD_2328724_3.pdf" V 2600 3350 50  0001 C CNN
	1    2350 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 3500 2900 3500
Wire Wire Line
	2900 3500 2900 3250
Wire Wire Line
	2900 3250 2150 3250
Wire Wire Line
	2150 3250 2150 3450
Wire Wire Line
	2150 3550 2200 3550
Wire Wire Line
	2200 3450 2150 3450
Connection ~ 2150 3450
Wire Wire Line
	2150 3450 2150 3550
Wire Wire Line
	2200 4450 2150 4450
Wire Wire Line
	2150 4450 2150 4350
Wire Wire Line
	2150 4350 2200 4350
Wire Wire Line
	2800 4400 2850 4400
Wire Wire Line
	2850 4400 2850 4850
Wire Wire Line
	2850 4850 2150 4850
Wire Wire Line
	2150 4850 2150 4450
Connection ~ 2150 4450
Wire Wire Line
	2800 3700 3800 3700
Wire Wire Line
	3550 3250 3550 3600
Connection ~ 2900 3250
$Comp
L misc:testpoint_1mm TP2
U 1 1 5C573A2D
P 2950 4650
F 0 "TP2" H 3100 4450 50  0000 L CNN
F 1 "testpoint_1mm" H 3150 4450 50  0001 L CNN
F 2 "kicad_pcb:TP_1mm" H 3150 4550 50  0001 L CNN
F 3 "" V 3200 4650 50  0001 C CNN
	1    2950 4650
	1    0    0    -1  
$EndComp
Connection ~ 2850 4850
Text Label 2700 4850 0    50   ~ 0
P2
Wire Wire Line
	2200 4650 2050 4650
Wire Wire Line
	2050 4650 2050 4750
Wire Wire Line
	2200 4550 2050 4550
Wire Wire Line
	2050 4550 2050 4650
Connection ~ 2050 4650
Wire Wire Line
	2200 4250 2050 4250
Wire Wire Line
	2050 4250 2050 4550
Connection ~ 2050 4550
Wire Wire Line
	2200 4150 2050 4150
Wire Wire Line
	2050 4150 2050 4250
Connection ~ 2050 4250
Wire Wire Line
	2050 4150 2050 4050
Wire Wire Line
	2050 3650 2200 3650
Connection ~ 2050 4150
Wire Wire Line
	2200 3750 2050 3750
Connection ~ 2050 3750
Wire Wire Line
	2050 3750 2050 3650
Wire Wire Line
	2200 3850 2050 3850
Connection ~ 2050 3850
Wire Wire Line
	2050 3850 2050 3750
Wire Wire Line
	2200 3950 2050 3950
Connection ~ 2050 3950
Wire Wire Line
	2050 3950 2050 3850
Wire Wire Line
	2200 4050 2050 4050
Connection ~ 2050 4050
Wire Wire Line
	2050 4050 2050 3950
$Comp
L misc:tooling_hole TH1
U 1 1 5C57F6F8
P 5050 4050
F 0 "TH1" H 5150 4050 50  0000 L CNN
F 1 "tooling_hole" H 5200 4050 50  0001 L CNN
F 2 "kicad_pcb:tooling_hole_2.55" H 5200 4150 50  0001 L CNN
F 3 "" V 5250 4250 50  0001 C CNN
	1    5050 4050
	1    0    0    -1  
$EndComp
$Comp
L misc:tooling_hole TH2
U 1 1 5C57F878
P 5050 4200
F 0 "TH2" H 5150 4200 50  0000 L CNN
F 1 "tooling_hole" H 5200 4200 50  0001 L CNN
F 2 "kicad_pcb:tooling_hole_2.55" H 5200 4300 50  0001 L CNN
F 3 "" V 5250 4400 50  0001 C CNN
	1    5050 4200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
