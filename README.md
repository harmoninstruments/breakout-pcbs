# Breakout PCBs

## [USB Type C](USB-C_breakout)
Provides access to the Super Speed pairs via 2.92 mm connectors.

![3D](USB-C_breakout/USB-C.png)

### [Connector Interface Simulation](https://github.com/dlharmon/pyopenems/tree/master/examples)

### PCB
4 layers, OSHPark standard stackup

### Parts
 - Hirose HK-R-SR2-1 (2.92 mm) or HRM(G)-300-468B-1 (SMA) or Rosenberger 03K721-40MS3 (3.5 mm)
 - 0-80 x 3/16 screws if using the Hirose connectors, included with the Rosenberger
 - JAE DX07S024WJ3R400 USB-C

## [FPC 21](FPC_21_breakout)
Breakout of the high speed differential pair of a Harmon Instruments inside box interconnect FPC.

![3D](FPC_21_breakout/3d.png)

### PCB
4 layers, OSHPark standard stackup
